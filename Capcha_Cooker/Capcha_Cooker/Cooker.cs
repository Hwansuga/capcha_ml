﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capcha_Cooker
{
    public partial class Cooker : Form
    {
        public Cooker()
        {
            InitializeComponent();
        }

        public void PrintLog(string msg_)
        {
            this.listBox_Log.Invoke(new MethodInvoker(delegate ()
            {
                this.listBox_Log.Items.Add(msg_);
            }));
        }


        private void Cooker_Load(object sender, EventArgs e)
        {

        }

        private void button_Split_Save_Path_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    PrintLog($"Source Path {fbd.SelectedPath}");

                    var projectDirectory = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "../../../"));
                    var assetsRelativePath = Path.Combine(projectDirectory, "assets\\NumberImg2");

                    string[] files = Directory.GetFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories);
                    PrintLog("분리 작업 시작");
                    Task task = new Task(() =>
                    {
                        foreach (var file in files)
                        {
                            if ((Path.GetExtension(file) != ".jpg") && (Path.GetExtension(file) != ".png"))
                                continue;
                            Bitmap temp = new Bitmap(file);
                            var label = Path.GetFileName(file).Replace(".jpg", "").Replace(".png", "");
                            var labelName = Path.GetFileName(Path.GetDirectoryName(file));


                            PrintLog($"{label} work , type : {labelName}");

                            List<Image> listRet = ModelTest.GetSplitImg(temp, labelName);
                            var arrLabel = label.ToCharArray();

                            for (int i = 0; i < listRet.Count; ++i)
                            {
                                listRet[i].Save($"{assetsRelativePath}\\{arrLabel[i]}\\{label}_{i}.png");
                            }


                        }
                        PrintLog("분리 작업 완료");
                    });
                    task.Start();
                }
            }
        }

        private void button_AnalyzeAll_Click(object sender, EventArgs e)
        {

            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    PrintLog("작업 시작");
                    Task task = new Task(() =>
                    {
                        int co = 0;
                        int wr = 0;
                        foreach (var file in files)
                        {
                            if ((Path.GetExtension(file) != ".jpg") && (Path.GetExtension(file) != ".png"))
                                continue;

                            Image temp = new Bitmap(file);
                            var label = Path.GetFileName(file);

                            string type = ModelTest.AnalyzeImgType(file);

                            List<Image> listRet = ModelTest.GetSplitImg(temp, type);
                            var ret = ModelTest.AnalyzeImg(listRet);
                            string fullString = string.Join("", ret.ToArray());

                            if (label.Contains(fullString))
                            {
                                //PrintLog("Correct!");
                                co++;
                            }
                            else
                            {
                                PrintLog($"{label} , type : {type} , result : {fullString} -- wrong");
                                var arrRet = fullString.ToCharArray();
                                var arrLabel = label.ToCharArray();
                                for (int i = 0; i < arrRet.Length; ++i)
                                {
                                    if (arrRet[i] != arrLabel[i])
                                    {
                                        listRet[i].Save($"WrongImg\\{label.Replace(".jpg", "")}_{arrRet[i]}.png");
                                    }
                                }
                                //System.IO.File.Copy(file, $"WrongImg\\{label}");
                                wr++;
                            }

                        }

                        PrintLog($"작업 완료  {co} / {wr}");
                    });
                    task.Start();

                }
            }
        }
    }
}
