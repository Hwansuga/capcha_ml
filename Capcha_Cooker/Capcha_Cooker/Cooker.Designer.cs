﻿namespace Capcha_Cooker
{
    partial class Cooker
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_Log = new System.Windows.Forms.ListBox();
            this.panel_Menu = new System.Windows.Forms.Panel();
            this.button_Split_Save_Path = new System.Windows.Forms.Button();
            this.button_AnalyzeAll = new System.Windows.Forms.Button();
            this.panel_Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox_Log
            // 
            this.listBox_Log.Dock = System.Windows.Forms.DockStyle.Right;
            this.listBox_Log.FormattingEnabled = true;
            this.listBox_Log.ItemHeight = 12;
            this.listBox_Log.Location = new System.Drawing.Point(434, 0);
            this.listBox_Log.Name = "listBox_Log";
            this.listBox_Log.Size = new System.Drawing.Size(274, 450);
            this.listBox_Log.TabIndex = 0;
            // 
            // panel_Menu
            // 
            this.panel_Menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(49)))), ((int)(((byte)(69)))));
            this.panel_Menu.Controls.Add(this.button_AnalyzeAll);
            this.panel_Menu.Controls.Add(this.button_Split_Save_Path);
            this.panel_Menu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_Menu.Location = new System.Drawing.Point(0, 0);
            this.panel_Menu.Name = "panel_Menu";
            this.panel_Menu.Size = new System.Drawing.Size(128, 450);
            this.panel_Menu.TabIndex = 1;
            // 
            // button_Split_Save_Path
            // 
            this.button_Split_Save_Path.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Split_Save_Path.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Split_Save_Path.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_Split_Save_Path.Location = new System.Drawing.Point(0, 0);
            this.button_Split_Save_Path.Name = "button_Split_Save_Path";
            this.button_Split_Save_Path.Size = new System.Drawing.Size(128, 23);
            this.button_Split_Save_Path.TabIndex = 8;
            this.button_Split_Save_Path.Text = "Split_Save_Path";
            this.button_Split_Save_Path.UseVisualStyleBackColor = true;
            this.button_Split_Save_Path.Click += new System.EventHandler(this.button_Split_Save_Path_Click);
            // 
            // button_AnalyzeAll
            // 
            this.button_AnalyzeAll.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_AnalyzeAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_AnalyzeAll.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_AnalyzeAll.Location = new System.Drawing.Point(0, 23);
            this.button_AnalyzeAll.Name = "button_AnalyzeAll";
            this.button_AnalyzeAll.Size = new System.Drawing.Size(128, 23);
            this.button_AnalyzeAll.TabIndex = 9;
            this.button_AnalyzeAll.Text = "AnalyzeAll";
            this.button_AnalyzeAll.UseVisualStyleBackColor = true;
            this.button_AnalyzeAll.Click += new System.EventHandler(this.button_AnalyzeAll_Click);
            // 
            // Cooker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(43)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(708, 450);
            this.Controls.Add(this.panel_Menu);
            this.Controls.Add(this.listBox_Log);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Cooker";
            this.Text = "Cooker";
            this.Load += new System.EventHandler(this.Cooker_Load);
            this.panel_Menu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_Log;
        private System.Windows.Forms.Panel panel_Menu;
        private System.Windows.Forms.Button button_Split_Save_Path;
        private System.Windows.Forms.Button button_AnalyzeAll;
    }
}

